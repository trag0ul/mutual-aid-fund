# Mutual Aid Fund
Using smart contracts to benefit the community. The idea of this is to provide a means of crowd support and funding,
but utilizing smart contracts. Essentially you join a community, then pay into the service whenever you can or monthly, 
and that affects your voting power in that community, proportional to total sum of money donated (maybe) divided by the total sum from everyone. 
Can then call votes if voters either agree to let you vote or you have the voting power from prior donations.  
A vote can be "hey my car broke down I need $200 this month" or something similar. 
Votes will have a deadline and some method to determine what to do if not enough voters have voted before then. 

## Roadmap
- Implement Smart Contracts with Solidity or Solana (probably in Rust)
- Create Login Screen
- Create ability to join communities
- Implement Subscribing and Donating
- Implement Voting and Requests

## Contributing


## Authors and acknowledgment
Chance Cardona 

## License
For open source projects, say how it is licensed.
